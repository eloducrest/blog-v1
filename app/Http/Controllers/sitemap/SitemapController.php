<?php

namespace App\Http\Controllers\sitemap;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index(Category $catgegory_slug)
    {
        $allcategories = Category::all();
        $posts = Post::all();
        return response()->view('sitemap.index', compact('allcategories', 'posts', 'catgegory_slug'))->header('Content-Type', 'text/xml');
    }
}
