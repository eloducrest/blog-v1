<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Contact;
use Carbon\Carbon;

class ModerationController extends Controller
{
    //
    //
    // RETURN VIEW COMMENTS NOT VALIDATE
    public function comments()
    {
        $allComments = Comment::all();
        $commentsToValidate = Comment::where('verified_at', '=', null)->get();

        return view('admin/moderation/comments', compact('commentsToValidate', 'allComments'));
    }

    // DELETE COMMENT
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect('admin/moderation-comments')->with('success_delete_comment','Le commentaire a bien été supprimé !');
    }

    // VALIDATE COMMENT
    public function verified(Comment $comment)
    {
        $comment->verified_at = Carbon::now();
        $comment->save();
        return back();
    }



    //
    //
    //ALL USERS MODERATION
    public function allusers()
    {
        // COMMENTS TABLE
        $uniqueUserComment = Comment::oldest()->get()
                            ->unique('email')->all();

        foreach ($uniqueUserComment as $comment)
        {
            $comment->totalCommentByUser = $comment->countSamesComment($comment->email);
            $comment->lastComment = $comment->getLastComment($comment->email);
        }


        // CONTACTS TABLE
        $uniqueUserContact = Contact::oldest()->get()
                            ->unique('email')->all();

        foreach ($uniqueUserContact as $contact)
        {
            $contact->totalContactByUser = $contact->countSamesContact($contact->email);
            $contact->lastContact = $contact->getLastContact($contact->email);
        }

        return view('admin/moderation/allusers', compact('uniqueUserComment', 'uniqueUserContact'));
    }
}
