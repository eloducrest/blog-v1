<?php

namespace App\Http\Controllers\admin;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*public function __construct()
    {
        $this->middleware('auth');
    }*/
    public function index()
    {
        $totalValidateComments      = Comment::where('verified_at', '!=', null)->count();
        $totalModerationComments    = Comment::where('verified_at', '=', null)->count();
        $uniqueUserComment          = Comment::oldest()->get()->unique('email')->count();
        $totalDraft                 = Post::where('is_published', '=', 0)->count();
        $totalPosts                 = Post::where('is_published', '=', 1)->count();
        return view('admin.index', compact('totalValidateComments', 'totalModerationComments', 'totalPosts', 'uniqueUserComment', 'totalDraft'));
    }

}
