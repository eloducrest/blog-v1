<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Instagram;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class InstagramController extends Controller
{
    public function show_insta()
    {
        $instagrams = Instagram::orderBy('created_at', 'desc')->get();

        return view('admin/settings/instagram', compact('instagrams'));
    }

    //
    //STORE AND DESTROY INSTA IMAGE
    public function store_insta(Request $request)
    {
        $request->validate([
            'file' => 'required',
            'alt_img_insta' => 'required',
            'link_to_insta' => 'required',
        ]);
        $instagram = new Instagram();
        $instagram->insta_img = $instagram->saveImageToDisk($request->file('file'), 'instagram', $request->alt_img_insta);
        $instagram->alt_img_insta = $request->alt_img_insta;
        $instagram->link_to_insta = $request->link_to_insta;

        $instagram->save();
        return redirect('/admin/manage-insta')->with('success_insta','Photo created successfully!');
    }


    public function destroy_insta(Instagram $instagram)
    {
        $instagram->delete();
        return redirect('/admin/manage-insta')->with('success_insta','Photo deleted successfully!');
    }
}
