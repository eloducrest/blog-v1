<?php

namespace App\Http\Controllers\admin;

use App\Core\FileSaving\FilesSavingClass;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\PostTag;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function allposts()
    {
        $posts = Post::where('is_published', '=', true)->with('category')->withCount('comments')->get();
        $postsDraft = Post::where('is_published', '=', false)->get();
        return view('admin.posts.allposts', compact('posts', 'postsDraft'));
    }

    public function create()
    {
        $tags = Tag::all();
        $categories = Category::all();

        return view('admin.posts.create', compact('categories', 'tags'));
    }

    public function store(Request $request)
    {
        /*$request->validate([
            'title' => 'required',
            'tag' => 'required',
            'category' => 'required',
            'main_img' => 'required',
            'body' => 'required',
        ]);*/

        $post = new Post();
        $post->title = $request->title;
        $post->category_id = $request->category;
        $post->main_img = $post->saveImageToDisk($request->file('file'), 'posts', $post->title);
        $post->body = $request->body;
        $post->chapo = $request->chapo;
        $post->meta_desc = $request->meta_desc;
        $post->alt_main_img = $request->alt_main_img;
        $post->location = $request->location;
        $post->slug = Str::slug($request->title, "-");

        if ($request->has('save'))
        {
            $post->is_published = false;
        }
        else if ($request->has('publish'))
        {
            $post->is_published = true;
        }

        $post->save();
        /*foreach ($request->tagsTet as $tag) {

            $postTag            = new PostTag();
            $postTag->post_id   = $post->id;
            $postTag->tag_id    = $tag;

            $postTag->save();
        }*/
        $post->tags()->sync($request->tagsTet);

        return redirect('/admin/allposts')->with('success', 'Post created successfully!');
    }

    public function show(Post $post)
    {
        $post = $post->load([
            'category',
            'tags',
            'comments.childs',
        ]);
        $totalComments = Comment::where('post_id', '=', $post->id)->count();

        return view('admin/posts/show', compact('post', 'totalComments'));
    }

    public function edit(Post $post)
    {
        $tags = Tag::all();
        $categories = Category::all();

        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    public function update(Request $request, Post $post)
    {
        /*$request->validate([
            'title' => 'required',
            'category_id' => 'required',
            //'main_img' => 'required',
            'body' => 'required',
            'chapo' => 'required',
            'meta_desc' => 'required',
            'alt_main_img' => 'required',
            'location' => 'required',
        ]);*/
        $post->title = $request->title;
        $post->category_id = $request->category;
        $post->body = $request->body;
        $post->chapo = $request->chapo;
        $post->meta_desc = $request->meta_desc;
        $post->alt_main_img = $request->alt_main_img;
        $post->location = $request->location;
        $post->slug = Str::slug($request->title, "-");

        if ($request->has('save'))
        {
            $post->is_published = false;
        }
        else if ($request->has('publish'))
        {
            $post->is_published = true;
        }

        $post->save();

        $post->main_img = $post->saveImageToDisk($request->file('file'), 'posts', $post->title);

        $post->tags()->sync($request->tagsTet);
        /* foreach ($request->tagsTet as $tag) {


             $postTag            = new PostTag();
             $postTag->post_id   = $post->id;
             $postTag->tag_id    = $tag;

             $postTag->save();
         }*/
        return redirect('/admin/allposts')->with('success', 'L\'article a bien été mis à jour !!!');
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect('/admin/allposts')->with('success', 'Post deleted successfully!');
    }

    //upload img (in summernote and input form)
    public function uploadImages(Request $request)
    {
        $fileSaving = new FilesSavingClass();
        return $fileSaving->saveImageToDisk($request->file('file'), 'posts', md5(rand(100, 200)));
    }
    /*    public function uploadImages(Request $request)
    {
        $file = $request->file('file');
        if ($request->title !== null) {
            $name = $request->title . '-' . Carbon::now()->format('d-m-Y');
        } else {
            $name = md5(rand(100, 200));
        }

        $filename = Str::slug($name) . '.' . $file->getClientOriginalExtension();
        $pathTemp = '/uploads/' . Carbon::now()->format('Y/m');

        Storage::putFileAs('public' . $pathTemp, $file, $filename);

        return $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/storage/' . $pathTemp . '/' . $filename;
    }
*/
}
