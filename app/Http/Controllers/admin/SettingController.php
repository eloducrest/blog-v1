<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Instagram;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    //
    //RETURN VIEW SETTING
    public function allsettings()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $instagrams = Instagram::orderBy('created_at', 'desc')->get();
        $categoriesParents = Category::where('parent_id', '=', null)->get();

        return view('admin/settings/settings', compact('categories', 'tags', 'categoriesParents', 'instagrams'));
    }




    //
    //STORE AND DESTROY CATEGORIES
    public function store_cat(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->category_slug = Str::slug($request->name, '-');

        $category->save();
        return redirect('/admin/settings')->with('success_cat','Category created successfully!');
    }


    public function destroy_cat(Category $category)
    {
        $category->delete();
        return redirect('/admin/settings')->with('success_cat','Category deleted successfully!');
    }




    //
    //STORE AND DESTROY TAGS
    public function store_tag(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $tag = new Tag();
        $tag->name = $request->name;

        $tag->save();
        //return $tag;
       return redirect('/admin/settings')->with('success_tag','TAG created successfully!');
    }

    public function store_tag_in_post_create(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $tag = new Tag();
        $tag->name = $request->name;

        $tag->save();
        return $tag;
        //return redirect('/admin/settings')->with('success_tag','TAG created successfully!');
    }

    public function destroy_tag(Tag $tag)
    {
        $tag->delete();
        return redirect('/admin/settings')->with('success_tag','Tag deleted successfully!');
    }




   /* //
    //STORE AND DESTROY INSTA IMAGE
    public function store_insta(Request $request)
    {
        $request->validate([
            'insta_img' => 'required',
            'alt_img_insta' => 'required',
            'link_to_insta' => 'required',
        ]);

        $instagram = new Instagram();
        $instagram->insta_img = $this->upImageSummernote($request);
        $instagram->alt_img_insta = $request->alt_img_insta;
        $instagram->link_to_insta = $request->link_to_insta;

        $instagram->save();
        return redirect('/admin/settings')->with('success_insta','Photo created successfully!');
    }


    public function destroy_insta(Instagram $instagram)
    {
        $instagram->delete();
        return redirect('/admin/settings')->with('success_insta','Photo deleted successfully!');
    }





    //upload img (for instagram image)
    public function upImageSummernote(Request $request)
    {
        $file = $request->file('insta_img');
        if ($request->title !== null) {
            $name = $request->title . '-' . Carbon::now()->format('d-m-Y');
        } else {
            $name = md5(rand(100, 200));
        }

        $filename = Str::slug($name) . '.' . $file->getClientOriginalExtension();

        $pathTemp = '/uploads/' . Carbon::now()->format('Y/m');

        Storage::putFileAs('public' . $pathTemp, $file, $filename);

        return $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/storage/' . $pathTemp . '/' . $filename;
    }*/
}
