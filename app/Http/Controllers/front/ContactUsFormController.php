<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use Mail;

class ContactUsFormController extends Controller {

    // Show Contact Form
    public function createForm(Request $request) {
        return view('front.contact');
    }

    // Store Contact Form data
    public function ContactUsForm(Request $request) {

        // Form validation
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            /*'g-recaptcha-response' => 'required|captcha'*/
        ]);

        //  Store data in database
        Contact::create($request->all());

        //  Send mail to admin
        \Mail::send('mail', array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_query' => $request->get('message'),
        ), function($message) use ($request){
            $message->from($request->email);
            $message->to('contact@faustineclerc.fr', $request->name)/*->subject($request->get('subject'))*/;
        });

        return back()->with('success_sendMail', 'Nous avons bien reçu votre message, Merci ! Nous allons vous répondre dans les plus brefs délais.');
    }

}
