<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Instagram;
use App\Models\Front;
use App\Models\Post;
use Illuminate\View\View;

class FrontController extends Controller
{
    public function homeposts()
    {
        $posts      = Post::with('category.parent')
            ->where('is_published', '=', 1)
            ->orderBy('created_at', 'desc')->get();
        $instagrams = Instagram::orderBy('created_at', 'desc')->get();

        return view('front/home', compact('posts', 'instagrams'));
    }

    public function show($category, Post $post)
    {
        $post = $post->load([
            'category',
            'tags',
            'comments.childs'
        ]);

        $next           = Post::where('id', '>', $post->id)->first();
        $previous       = Post::where('id', '<', $post->id)->orderBy('id', 'desc')->first();

        $totalComments  = Comment::where('post_id', '=', $post->id)->where('verified_at', '!=', null)->count();

        return view('front/post', compact('post', 'next', 'previous', 'totalComments'));
    }

    public function postByCat(Category $category)
    {
        $catId      = $category->id;
        $posts      = Post::with('category.parent')->where('is_published', '=', 1)
                            ->where('category_id', '=', $category->id)
                            ->orderBy('created_at', 'desc')->get();
        $instagrams = Instagram::orderBy('created_at', 'desc')->get();

        return view('front/home', compact('posts', 'catId', 'instagrams', 'category'));
    }

    public function mentionslegales() {
        return view('front.mentions-legales');
    }
}
