<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        /* Rajouter requete de validation comme store_tag
            https://laravel.com/docs/8.x/validation#available-validation-rules
        */
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'body' => 'required'
        ]);


        $comment = new Comment();

        $comment->email     = $request->email;
        $comment->name      = $request->name;
        $comment->body      = $request->body;
        $comment->post_id   = $request->post_id;
        $comment->parent_id = $request->parent_id;


        $user_verified = Comment::where('email', '=', $request->email)->where('verified_at', '!=', null)->first();
        if ($user_verified !== null){
            $comment->verified_at = Carbon::now();
        }

        $comment->save();

        if ($user_verified !== null){
            return redirect()->back()
                ->with('success_comment', 'Votre commentaire a bien été plublié ! Merci beaucoup');
        }

        return redirect()->back()
            ->with('success_comment','Merci pour votre commentaire, nous l\'avons bien reçu. Il est en cours de modération.');
    }
}
