<?php

namespace App\Models;

use App\Core\FileSaving\FilesSaving;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    use HasFactory;
    use FilesSaving;
}
