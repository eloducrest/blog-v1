<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function childs()
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id')
            ->where(function ($query){
                return  $query->where('verified_at', '!=', null)
                    ->orWhere('email', '=', 'faustine.clerc1@gmail.com');
            });
    }

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }

    public function countSamesComment($email){
        return Comment::where('email', '=', $email)->count();
    }

    public function getLastComment($email){
        return Comment::where('email', '=', $email)->select('created_at')->latest()->first();
    }
}
