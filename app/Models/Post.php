<?php

namespace App\Models;

use App\Core\FileSaving\FilesSaving;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Post extends Model
{
    use HasFactory;
    use FilesSaving;

    protected $fillable = ['title', 'category_id', 'main_img', 'body', 'chapo', 'meta_desc', 'alt_main_img', 'location', 'slug'];

    public function category(): HasOne
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'posts_tags', 'post_id','tag_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id', 'id')
            ->where('parent_id', '=', null)

            ->where(function ($query){
                return  $query->where('verified_at', '!=', null)
                    ->orWhere('email', '=', 'faustine.clerc1@gmail.com');
            });
    }
}
