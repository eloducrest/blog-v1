<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    public $fillable = ['name', 'email', 'message'];

    public function countSamesContact($email){
        return Contact::where('email', '=', $email)->count();
    }

    public function getLastContact($email){
        return Contact::where('email', '=', $email)->select('created_at')->latest()->first();
    }
}
