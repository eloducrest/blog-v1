<?php

namespace App\Core\FileSaving;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait FilesSaving
{
    /**
     * Save the current image
     *
     * @param null $file the file
     * @param null $diskName
     * @param null $resource_name the resource name
     *
     * @return boolean|string the image’s path or false on failure
     */
    public function saveImageToDisk($file = null, $diskName = null, $resource_name = null)
    {
        // If a file has been uploaded
        if ($file !== null) {
            $filename = Str::slug($resource_name).'.'.$file->getClientOriginalExtension();

            $disk = Storage::disk($diskName);
            $disk->put(Carbon::now()->format('Y/m/') . $filename, fopen($file, 'r+'));

            return $disk->url(Carbon::now()->format('Y/m/') .$filename);
        }

        return false;
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function removeImageFromDisk(string $path)
    {
        return Storage::delete($path);
    }
}