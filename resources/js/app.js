require('./bootstrap');

require('alpinejs');

/*
|--------------------------------------------------------------------------
|       FRONT ONLY
|--------------------------------------------------------------------------
|
|
|
*/
    //SCRIPT FOR NAV STICKY WHEN SCROLLING
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
};



    //SCRIPT FOR HIDE & SHOW HOME ICON ON STICKY NAVBAR AFTER SCROLL 240PX
myID = document.getElementById("home-icon");

var myScrollFunc = function () {
  var y = window.scrollY;
  if (y >= 240) { //
    myID.className = "bottomMenu show";
  } else {
    myID.className = "bottomMenu hide";
  }
};

window.addEventListener("scroll", myScrollFunc);



    // SCRIPT FOR HIDE AND SHOW SEARCH BAR IN NAV
$(document).ready(function(){
  $(".toggle-search").click(function(){
    $(".search-input").toggle();
  });
});




  //  SCRIPT FOR FORM CONTACT AND COMMENT
$('.input2').each(function(){
  $(this).on('blur', function(){
    if($(this).val().trim() != "") {
      $(this).addClass('has-val');
    }
    else {
      $(this).removeClass('has-val');
    }
  })
})




/*
|--------------------------------------------------------------------------
|       ADMIN ONLY
|--------------------------------------------------------------------------
|
|
|
*/







/*
|--------------------------------------------------------------------------
|       ADMIN AND FRONT
|--------------------------------------------------------------------------
|
|
|
*/


