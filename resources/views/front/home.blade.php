@extends('app')
@section('description', 'Accueil du blog de Faustine Clerc. Ce blog sera un condensé de tous ces sujets : la mode, la décoration, les bonnes adresses, mes découvertes en somme !')

@section('content')

    <section class="featured-posts no-padding-top">
        <div class="container">

            @if(request()->routeIs('home'))
                <h2 class="text-center mb-5 home-cat-title">Les derniers articles</h2>
            @elseif(request()->fullUrlIs('https://faustineclerc.fr/a-propos'))
                <h2 class="text-center mb-5 home-cat-title">À propos</h2>
            @else
                <h2 class="text-center mb-5 home-cat-title">Les articles de <span class="text-lowercase">{{ $category->name }}</span></h2>
            @endif

            @if(count($posts) === 0)
                <p style="margin-bottom: 5rem;">Désolée, il n'y a pas encore d'article dans cette catégorie</p>
            @endif


            <!-- POST -->
            @foreach($posts as $index => $post)

                <div class="row align-items-stretch mb-3 mt-3">
                   @if($index&1)
                        <div class="text col-12 col-md-6 my-auto">
                            <div class="content text-center">

                                <span class="category">
                                    {{ $post->category->parent ?  $post->category->parent->name . ' : ' . $post->category->name : $post->category->name  }}
                                </span>
                                <a href="{{ $post->category->category_slug }}/{{ $post->slug }}" class="post-title">
                                    <h2>{{ $post->title }}</h2>
                                </a>
                                <p>{{ $post->chapo }}</p>
                                <div class="post-footer text-center align-items-center">
                                    <div class="d-inline-flex">
                                        <div class="date">
                                            <i class="fa fa-calendar"></i>{{ date('d-M-Y', strtotime($post->created_at)) }}
                                        </div>
                                        <div class="comments">
                                            <i class="fa fa-map-marker"></i> {{ $post->location }}
                                        </div>
                                    </div>
                                    <div class="mt-3">
                                        <a href="{{ $post->category->category_slug }}/{{ $post->slug }}" class="btn">
                                            Voir l'article
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="image col-12 col-md-6">
                            <a href="{{ $post->category->category_slug }}/{{ $post->slug }}">
                                <img src="{!! $post->main_img !!}" alt="{{ $post->alt_main_img }}">
                            </a>
                        </div>

                    @else
                        <div class="image col-12 col-md-6">
                            <a href="{{ $post->category->category_slug }}/{{ $post->slug }}">
                                <img src="{!! $post->main_img !!}" alt="{{ $post->alt_main_img }}">
                            </a>
                        </div>
                        <div class="text col-12 col-md-6 my-auto">
                            <div class="content text-center">
                                <span class="category">
                                    {{ $post->category->parent ?  $post->category->parent->name . ' : ' . $post->category->name : $post->category->name  }}
                                </span>
                                <a href="{{ $post->category->category_slug }}/{{ $post->slug }}" class="post-title">
                                    <h2 class="h3">{{ $post->title }}</h2>
                                </a>
                                <p>{{ $post->chapo }}</p>
                                <div class="post-footer text-center align-items-center">
                                    <div class="d-inline-flex">
                                        <div class="date">
                                            <i class="fa fa-calendar"></i> {{ date('d-M-Y', strtotime($post->created_at)) }}
                                        </div>
                                        <div class="comments">
                                            <i class="fa fa-map-marker"></i> {{ $post->location }}
                                        </div>
                                    </div>
                                    <div class="mt-3">
                                        <a href="{{ $post->category->category_slug }}/{{ $post->slug }}" class="btn">
                                            Voir l'article
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                   @endif
                </div>
            @endforeach
        </div>
    </section>

    <section class="insta-footer portfolio">
        <div class="container-fluid">

            <div class="text-center">
                <a href="https://www.instagram.com/faustine.clerc/" target="_blank" aria-label="instagram" title="instagram">
                    <h3>Mon Instagram</h3>
                </a>
            </div>

            <div class="row mt-5">
                @foreach($instagrams as $instagram)
                    <div class="img-insta-content text-center col-3 col-lg col-md col-sm-3">
                        <div class="img-insta-footer">
                            <a href="{{ $instagram->link_to_insta }}" target="_blank">
                                <img class="img-cropped" src="{!! $instagram->insta_img !!}" alt="{{ $instagram->alt_img_insta }}">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>
@endsection
