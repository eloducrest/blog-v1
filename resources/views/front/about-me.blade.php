@extends('app', ['title' => 'About Me'])

@section('content')
    <div class="about-header">
        <h1 class="text-center" style="margin-top: -100px;">About Me</h1>
        <img class="img-center" src="{{ asset('img/faustine-aboutme.png') }}">
    </div>
@endsection
