@extends('app', ['title' => $post->title])
@section('description', $post->meta_desc)

@section('content')

    
    <div class="container">
        <div class="post-meta">
            <div class="row">
                <div class="col-sm-10 offset-sm-1">
                    <div class="path">
                        <a href="/">
                            <i class="fa fa-home"></i>
                        </a>
                        <i class="fa fa-chevron-right"></i>
                        <a class="melesa-max-limit" href="/{{ $post->category->category_slug }}">
                            <span>{{ $post->category->name }}</span>
                        </a>
                        <i class="fa fa-chevron-right"></i>
                        <a class="melesa-max-limit" style="text-decoration: none;">
                            <span>{{ $post->title }}</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-6">
                    @if (session('success_comment'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_comment') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <section class="post-intro-content">
            <div class="row">
                <div class="col-md-6 my-auto">
                    <div class="post-header-content text-center my-auto">
                        <div class="post-meta">
                            <div class="category">
                                <a href="/{{ $post->category->category_slug }}">
                                    {{ $post->category->parent ?  $post->category->parent->name . ' : ' . $post->category->name : $post->category->name  }}
                                </a>
                            </div>
                            <h1 class="my-4">{{ $post->title }}</h1>
                            <div class="d-inline-flex">
                                <div class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{ date('d M Y', strtotime($post->created_at)) }}
                                </div>
                                <div class="comments">
                                    <i class="fa fa-map-marker"></i> {{ $post->location }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="post-thumbnail">
                        <img src="{!! $post->main_img !!}" alt="{{ $post->alt_main_img }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>


        <main class="post blog-post col-lg-12">
            <div class="post-single">
                <div class="post-details">
                    @if(request()->fullUrlIs('https://faustineclerc.fr/decoration/dressing-amenagement-conseils-et-astuces'))
                        <div class="row summary-post">
                            <div class="col-md-3">
                                <div class="sum-post">
                                    <ol>
                                        <li class="nav-item-post"><a onclick="goToSection(1)">L’aménagement d’un dressing</a></li>
                                        <li class="nav-item-post"><a onclick="goToSection(2)">Conseils et astuces</a></li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="post-body">
                                    {!! $post->body !!}
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="post-body">
                            {!! $post->body !!}
                        </div>
                    @endif

                    <div class="post-tags">
                        @foreach($post->tags as $tag)
                            <span style="margin-right: 10px;"><a style="color: #3490dc; text-decoration: none; background-color: transparent;">#{{ $tag->name }}</a></span>
                        @endforeach
                    </div>


                    <!-- PREVIOUS & NEXT POST NAVIGATION -->
                    <div class="posts-nav d-flex justify-content-between align-items-stretch flex-column flex-md-row">
                        @if (isset($previous))
                            <a href="/{{ $previous->category->name }}/{{ $previous->slug }}" class="prev-post text-left d-flex align-items-center">
                                <div class="icon prev">
                                    <i class="fa fa-angle-left"></i>
                                </div>
                                <div class="text">
                                    <strong class="next-previous-posts">Article précédent </strong>
                                    <h6>{{$previous->title}}</h6>
                                </div>
                            </a>
                        @else
                            <div></div>
                        @endif

                        @if (isset($next))
                            <a href="/{{ $next->category->name }}/{{ $next->slug }}" class="next-post text-right d-flex align-items-center justify-content-end">
                                <div class="text">
                                    <strong class="next-previous-posts">Article suivant </strong>
                                    <h6>{{$next->title}}</h6>
                                </div>
                                <div class="icon next">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        @endif
                    </div>
                    <!-- /. PREVIOUS & NEXT POST NAVIGATION -->


                    <!-- COMMENTS -->
                    <div class="post-comments">
                        <header>
                            <h4>Commentaires<span class="no-of-comments">({{ $totalComments }})</span></h4>
                        </header>

                        @foreach($post->comments as $comment)

                            <div class="comment">
                                <div id="{{ $comment->id }}" class="comment-parent">
                                    <div class="comment-header d-flex justify-content-between">
                                        <div class="user d-flex align-items-center">
                                            <div class="image">
                                                @if($comment->email === 'contact@faustineclerc.fr')
                                                    <img src="{{ asset('img/logo-de-faustine-clerc-removebg.png') }}" alt="..."
                                                         class="img-fluid rounded-circle">
                                                @else
                                                    <img src="{{ asset('img/user.svg') }}" alt="..."
                                                         class="img-fluid rounded-circle">
                                                @endif
                                            </div>
                                            <div class="title">
                                                <strong>{{ $comment->name }}</strong>
                                                <span class="date">{{ date('d M Y', strtotime($comment->created_at)) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment-body">
                                        <p>{{ $comment->body }}</p>
                                    </div>
                                    <div class="comment-footer">
                                        <span class="comment-reply">
                                            {{--@if(count($comment->childs) === 0)--}}
                                                <a class="comment-reply-link btn" onclick="showFt({{ $comment->id }}, '{{ $comment->name }}')">
                                                    <i class="fa fa-reply"></i>
                                                    Répondre
                                                </a>
                                            {{--@endif--}}
                                        </span>
                                    </div>
                                </div>
                                @foreach($comment->childs as $child)
                                    <div id="{{ $child->id }}" class="comment-children">
                                        <div class="comment-header d-flex justify-content-between">
                                            <div class="user d-flex align-items-center">
                                                <div class="image">
                                                    @if($child->email === 'contact@faustineclerc.fr')
                                                        <img src="{{ asset('img/logo-de-faustine-clerc-removebg.png') }}" alt="..."
                                                             class="img-fluid rounded-circle">
                                                    @else
                                                        <img src="{{ asset('img/user.svg') }}" alt="..."
                                                             class="img-fluid rounded-circle">
                                                    @endif
                                                </div>
                                                <div class="title">
                                                    <strong>{{ $child->name }}</strong>
                                                    <span class="date">{{ date('d M Y', strtotime($child->created_at)) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="comment-body">
                                            <p><span class="reply-id">@ {{ $comment->name }}</span>, {{ $child->body }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    <!-- /. COMMENTS -->

                    <!-- ADD COMMENT OF COMMENTS -->
                    <div id="reply-form" style="display: none;">
                        <div id="reply-add-comment">
                            <header id="reply-add-comment-header">
                                <div class="reply-add-comment-h5" style="display: flex;">
                                    <h5 class="my-auto" id="response_at"></h5>
                                    <h5 id="close" class="my-auto" onclick="hideFt()">Annuler la rponse</h5>
                                </div>
                                <p>Votre adresse e-mail ne sera pas publiée. Les champs obligatoires sont indiqués avec un *</p>
                            </header>
                            <form action="{{ route('comment.store') }}" method="post" class="commenting-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <div class="wrap-input2 validate-input" data-validate="Name is required">
                                            <input class="input2" type="text" name="name">
                                            <span class="focus-input2" data-placeholder="NOM *"></span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <div class="wrap-input2 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                                            <input class="input2" type="text" name="email">
                                            <span class="focus-input2" data-placeholder="E-MAIL *"></span>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="wrap-input2 validate-input" data-validate="Message is required">
                                            <textarea class="input2" name="body"></textarea>
                                            <span class="focus-input2" data-placeholder="MESSAGE *"></span>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <button type="submit" class="btn">Laisser votre commentaire</button>
                                        <input type="hidden" value="{{ $post->id }}" name="post_id">
                                        <input type="hidden" value="" name="parent_id" id="parent_id">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /. ADD COMMENTS OF COMMENTS -->

                    <!-- ADD COMMENTS -->
                    <div id="add-comment">
                        <header id="add-comment-header">
                            <h5 class="h3">Laisser un commentaire</h5>
                            <p style="font-size: 12px; font-style: italic;">Votre adresse e-mail ne sera pas publiée. Les champs obligatoires sont indiqués avec un *</p>
                        </header>
                        <form action="{{ route('comment.store') }}" method="post" class="commenting-form">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <div class="wrap-input2 validate-input" data-validate="Name is required">
                                        <input class="input2" type="text" name="name" required>
                                        <span class="focus-input2" data-placeholder="NOM *"></span>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="wrap-input2 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                                        <input class="input2" type="text" name="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" title="example@gmail.com">
                                        <span class="focus-input2" data-placeholder="E-MAIL *"></span>
                                    </div>
                                </div>

                                <div class="col-md-12 form-group">
                                    <div class="wrap-input2 validate-input" data-validate="Message is required">
                                        <textarea class="input2" name="body" required></textarea>
                                        <span class="focus-input2" data-placeholder="MESSAGE *"></span>
                                    </div>
                                </div>

                                <div class="col-md-12 form-group">
                                    <button type="submit" class="btn">Laisser votre commentaire</button>
                                    <input type="hidden" value="{{ $post->id }}" name="post_id">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /. ADD COMMENTS -->

                </div>
            </div>
        </main>
    </div>

@endsection
