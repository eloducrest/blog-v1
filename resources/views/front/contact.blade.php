@extends('app', ['title' => 'Contact'])

@section('content')
    <div class="form-content">
        <div class="container">
            <div class="contact-title-content">
                @if (session('success_sendMail'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_sendMail') }}
                    </div>
                @endif
                <h1 class="text-center mb-3">Contactez-moi</h1>
            </div>
            <div class="row pt-5 mb-5">
                <div class="col-sm-12 col-lg-5 col-md-12">
                    <div class="contact-info-content">
                        <p>N'hésitez pas à m'envoyer un mail, je vous contacterai dès que possible. <br>D'ici là,
                            vous pouvez me suivre sur mes reseaux sociaux. <br>Passez une bonne journée.</p>
                        <div class="social-link my-5">
                            <a href="https://www.instagram.com/faustine.clerc/" target="_blank"
                               aria-label="instagram" title="instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.pinterest.fr/Faustine_Clerc/" target="_blank"
                               aria-label="pinterest" title="pinterest">
                                <i class="fa fa-pinterest"></i>
                            </a>
                            <a href="mailto:contact@faustineclerc.fr" aria-label="mailto" title="mailto">
                                <i class="fa fa-paper-plane"></i>
                            </a>
                        </div>
                        <h6 class="h2">Questions & collaboration</h6>
                        <p>Pour toute demande de collaboration, ou si tout simplement vous avez des questions, n'hésitez pas à m'écrire à : <b>contact@faustineclerc.fr</b></p>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-7 col-md-12">
                    <form class="contact2-form validate-form" method="post" action="{{ route('contact.store') }}" role="form">
                        @csrf

                        <div class="wrap-input2 validate-input" data-validate="Name is required">
                            <input class="input2" type="text" name="name">
                            <span class="focus-input2" data-placeholder="NOM *"></span>
                        </div>

                        <div class="wrap-input2 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                            <input class="input2" type="text" name="email">
                            <span class="focus-input2" data-placeholder="E-MAIL *"></span>
                        </div>

                        <div class="wrap-input2 validate-input" data-validate="Message is required">
                            <textarea class="input2" name="message"></textarea>
                            <span class="focus-input2" data-placeholder="MESSAGE *"></span>
                        </div>
                        <div style="font-size: 12px; font-style: italic;">
                            Les champs obligatoires sont indiqués avec un *
                        </div>

                        <div class="container-contact2-form-btn">
                            <div class="wrap-contact2-form-btn">
                                <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                    <div class="col-md-6 pull-center" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;">
                                        {!! app('captcha')->display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <button type="submit" class="contact2-form-btn btn">Envoyer votre message</button>
                            </div>
                        </div>
                    </form>
                </div>
                {!! NoCaptcha::renderJs() !!}
            </div>
        </div>
    </div>
@endsection
