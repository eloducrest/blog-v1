<div style="background-color: #FFFFFF; min-height: 100vh;">
    <div class="text-center">
        {{ $logo }}
    </div>

    <div class="text-center">
        {{ $slot }}
    </div>
</div>
