<button {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rgray-900 on-150']) }}>
    {{ $slot }}
</button>
