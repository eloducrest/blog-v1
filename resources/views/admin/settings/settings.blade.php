@extends('admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-6">
                @if (session('success_cat'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_cat') }}
                    </div>
                @endif

                @if (session('success_tag'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_tag') }}
                    </div>
                @endif

                @if (session('success_insta'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_insta') }}
                    </div>
                @endif
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="h2 card-header">{{ __('Gerer les categories') }}</div>

                    <form action="{{ url('admin/store-cat') }}" method="POST" style="margin-bottom: 1rem;">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Ajouter une categorie...">
                        </div>
                        <div class="form-group">
                            <select name="parent_id" id="parent_id">
                                <option value="">Faut-il mettre un parent à la catégorie ?</option>
                                @foreach($categoriesParents as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-green ml-3">Enregistrer</button>
                    </form>

                    <div class="card-body">
                        <table class="table table-bordered sortDataTable">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Catégorie</th>
                                <th>Catégorie parent</th>
                                <th>Cree le</th>
                                <th class="text-center">Supprimer</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->parent_id }}</td>
                                    <td>{{ date('d-m-y', strtotime($category->created_at)) }}</td>
                                    <td class="text-center">
                                        <form action="delete-cat/{{$category->id}}" method="post"  onsubmit="return confirm('Veux-tu vraiment delete la categorie belle gosse ?');" class="d-inline">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <button class="btn" type="submit">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

            <div class="col-md-6">
                <div class="card">

                    <div class="h2 card-header">{{ __('Gerer les tags') }}</div>

                    <form action="{{ url('admin/store-tag') }}" method="POST" style="margin-bottom: 1rem;">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Ajouter un tag...">
                        </div>
                        <button type="submit" class="btn btn-green ml-3">Enregistrer</button>
                    </form>

                    <div class="card-body">
                        <table class="table table-bordered sortDataTable">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Tag</th>
                                <th>Cree le</th>
                                <th class="text-center">Supprimer</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($tags as $tag)
                                <tr>
                                    <td>{{ $tag->id }}</td>
                                    <td>{{ $tag->name }}</td>
                                    <td>{{ date('d-m-y', strtotime($tag->created_at)) }}</td>
                                    <td class="text-center">
                                        <form action="delete-tag/{{$tag->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment delete le tag belle gosse ?')" class="d-inline">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <button class="btn" type="submit">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection
