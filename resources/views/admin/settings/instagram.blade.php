@extends('admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-6">
                @if (session('success_cat'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_cat') }}
                    </div>
                @endif

                @if (session('success_tag'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_tag') }}
                    </div>
                @endif

                @if (session('success_insta'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_insta') }}
                    </div>
                @endif
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="h2 card-header">{{ __('Gérer les photos Instagram') }}</div>

                    <form action="{{ url('admin/store-insta') }}" method="post" enctype="multipart/form-data" style="margin-bottom: 1rem;">
                        @csrf
                        <div class="form-group p-3">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="insta_img">Image Instagram à mettre</label>
                                    <input type="file" name="file" id="file" class="form-control-file">
                                </div>
                                <div class="col-md-3">
                                    <label for="alt_img_insta">Texte alt</label>
                                    <input type="text" name="alt_img_insta" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label for="name_img">Nom de l'image</label>
                                    <input type="text" name="name_img" class="form-control" disabled>
                                </div>
                                <div class="col-md-3">
                                    <label for="link_to_insta">Lien Instagram de l'image</label>
                                    <input type="text" name="link_to_insta" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-green ml-3">Enregistrer</button>
                    </form>
                </div>

                <div class="row mt-5">
                    @foreach($instagrams as $instagram)
                        <div class="img-insta-content text-center col-3 col-lg col-md col-sm-3">
                            <div class="img-insta-footer">
                                <a href="{{ $instagram->link_to_insta }}" target="_blank">
                                    <img class="img-cropped" src="{!! $instagram->insta_img !!}" alt="{{ $instagram->alt_img_insta }}">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="card-body">
                    <table class="table table-bordered sortDataTable">

                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Image</th>
                            <th>Texte alt</th>
                            <th>Lien Instagram</th>
                            <th>Image upload le</th>
                            <th class="text-center">Supprimer</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($instagrams as $instagram)
                            <tr>
                                <td>{{ $instagram->id }}</td>
                                <td><img src="{!! $instagram->insta_img !!}" alt="{{ $instagram->alt_img_insta }}" style="width: 100px;"></td>
                                <td>{{ $instagram->alt_img_insta }}</td>
                                <td><a href="{{ $instagram->link_to_insta }}">{{ $instagram->link_to_insta }}</a></td>
                                <td>{{ date('d-m-y', strtotime($instagram->created_at)) }}</td>
                                <td class="text-center">
                                    <form action="delete-insta/{{$instagram->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment delete la photo belle gosse ?');" class="d-inline">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button class="btn" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
