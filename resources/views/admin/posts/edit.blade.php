@extends('admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Modifier l\'article') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/admin/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Titre de l'article</label>
                            <input type="text" name="title" class="form-control" minlength="30" maxlength="60" value="{{$post->title}}">
                        </div>

                        <div class="form-group">
                            <label for="title">Chapeau de l'article</label>
                            <input type="text" name="chapo" class="form-control" maxlength="200" value="{{$post->chapo}}">
                        </div>

                        <div class="form-group">
                            <label for="meta_title">Meta title</label>
                            <input type="text" name="meta_title" class="form-control" disabled>
                        </div>

                        <div class="form-group">
                            <label for="meta_desc">Meta description</label>
                            <input type="text" name="meta_desc" class="form-control" maxlength="200" value="{{$post->meta_desc}}">
                        </div>

                        <div class="form-group">
                            <label for="location">Lieu de l'article</label>
                            <input type="text" name="location" class="form-control" value="{{$post->location}}">
                        </div>

                        <div class="form-group">
                            <label for="tag">Tag</label>
                            <select class="form-control js-example-tags" multiple="multiple" name="tagsTet[]">
                                @foreach($tags as $tag)
                                    @foreach($post->tags as $actual_tag)
                                        @if($actual_tag->id === $tag->id)
                                            <option value="{{ $tag->id }}" selected="selected">{{ $tag->name }}</option>
                                        @else
                                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="category">Catégories</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">--Choisir une catégorie--</option>
                                @foreach($categories as $key => $category);
                                    @if($category->id === $post->category_id)
                                        <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="image">Image de presentation</label>
                                    <input type="file" name="file" id="file" class="form-control-file" value="{{ $post->main_img }}">
                                </div>
                                <div class="col-md-4">
                                    <label for="alt_img">Texte alt</label>
                                    <input type="text" name="alt_main_img" class="form-control" value="{{$post->alt_main_img}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="name_img">Nom de l'image</label>
                                    <input type="text" name="name_img" class="form-control" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="h3" for="body">Contenu de l'article</label>
                            <textarea name="body" id="" cols="30" rows="10" class="textarea">{!! $post->body !!}</textarea>
                        </div>

                        <input type="submit" class="btn btn-green" name="publish" value="Publier">
                        <input type="submit" class="btn btn-green" name="draft" value="Enregistrer comme brouillon">

                        {{--<button type="submit" class="btn btn-green">Mettre à jour</button>--}}
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
