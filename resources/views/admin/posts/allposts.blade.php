@extends('admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 mt-4">
                <a href="{{ route('create') }}" class="btn btn-green mb-2" style="width: 50%;">Create Post</a>
            </div>
        </div>
        <div class="card">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                <br/>
            @endif
            <div class="card-header">
                <h2>Les articles publiés</h2>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered sortDataTable">

                    <thead>
                    <tr>
                        <th>Img</th>
                        <th>Title</th>
                        <th>Tag</th>
                        <th>Categorie</th>
                        <th>Contenu</th>
                        <th>Commentaires</th>
                        <th>Cree le</th>
                        <th>Modifier le</th>
                        <th class="text-center">Voir</th>
                        <th class="text-center">Modif</th>
                        <th class="text-center">Suppr</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td style="width: 150px;">
                                <img src="{!! $post->main_img !!}" alt="{{ $post->alt_main_img }}" style="width: 100%;">
                            </td>
                            <td style="width: 150px;">{{ $post->title }}</td>
                            <td>
                                @foreach($post->tags as $tag)
                                    <span style="margin-right: 10px;"><a href="#">#{{ $tag->name }} </a></span><br>
                                @endforeach
                            </td>
                            <td>{{ $post->category ? $post->category->name : '' }}</td>
                            <td style="width: 300px;">{!! Str::limit($post->body, 400) !!}</td>
                            <td class="text-center">{{ $post->comments_count}}</td>
                            <td>{{ date('d-m-y H:i', strtotime($post->created_at)) }}</td>
                            <td>{{ date('d-m-y H:i', strtotime($post->updated_at)) }}</td>
                            <td class="text-center">
                                <a href="post/{{$post->id}}" class="btn">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="post/{{$post->id}}/edit" class="btn">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <form action="post/{{$post->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment delete le post belle gosse ?')" class="d-inline">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button class="btn" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->


        <div class="card">
        <div class="card-header">
            <h2>Les brouillons</h2>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th>Img</th>
                            <th>Title</th>
                            <th>Tag</th>
                            <th>Categorie</th>
                            <th>Contenu</th>
                            <th>Cree le</th>
                            <th>Modifier le</th>
                            <th class="text-center">Voir</th>
                            <th class="text-center">Modif</th>
                            <th class="text-center">Suppr</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($postsDraft as $post)
                        <tr>
                            <td style="width: 150px;">
                                <img src="{!! $post->main_img !!}" alt="{{ $post->alt_main_img }}" style="width: 100%;">
                            </td>
                            <td style="width: 150px;">{{ $post->title }}</td>
                            <td>
                                @foreach($post->tags as $tag)
                                    <span style="margin-right: 10px;"><a href="#">#{{ $tag->name }} </a></span><br>
                                @endforeach
                            </td>
                            <td>{{ $post->category ? $post->category->name : '' }}</td>
                            <td style="width: 300px;">{!! Str::limit($post->body, 400) !!}</td>
                            <td>{{ date('d-m-y H:i', strtotime($post->created_at)) }}</td>
                            <td>{{ date('d-m-y H:i', strtotime($post->updated_at)) }}</td>
                            <td class="text-center">
                                <a href="post/{{$post->id}}" class="btn">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="post/{{$post->id}}/edit" class="btn">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <form action="post/{{$post->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment delete le post belle gosse ?')" class="d-inline">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button class="btn" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </div>

    <script>
        $(document).ready(function() {
            $('#example1').DataTable({
                "responsive": true,
                "autoWidth": false,
                "order": [[ 5, "desc" ]]
            });
        } );
    </script>
@endsection
