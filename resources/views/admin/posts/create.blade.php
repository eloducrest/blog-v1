@extends('admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Écrire un article') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/admin/post" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Titre de l'article</label>
                            <input type="text" name="title" class="form-control" minlength="30" maxlength="60">
                        </div>

                        <div class="form-group">
                            <label for="chapo">Chapeau de l'article</label>
                            <input type="text" name="chapo" class="form-control" maxlength="200">
                        </div>

                        <div class="form-group">
                            <label for="meta_title">Meta title</label>
                            <input type="text" name="meta_title" class="form-control" disabled>
                        </div>

                        <div class="form-group">
                            <label for="meta_desc">Meta description</label>
                            <input type="text" name="meta_desc" class="form-control" maxlength="200">
                        </div>

                        <div class="form-group">
                            <label for="location">Lieu de l'article</label>
                            <input type="text" name="location" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="tag">Tag</label>
                            <select class="form-control js-example-tags" multiple="multiple" name="tagsTet[]">
                                @foreach($tags as $tag);
                                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="category">Catégories</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">--Choisir une catégorie--</option>
                                @foreach($categories as $key => $category);
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="image">Image de présentation</label>
                                    <input type="file" name="file" id="file" class="form-control-file">
                                </div>
                                <div class="col-md-4">
                                    <label for="alt_main_img">Texte alt</label>
                                    <input type="text" name="alt_main_img" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label for="name_img">Nom de l'image</label>
                                    <input type="text" name="name_img" class="form-control" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="h3" for="">Contenu de l'article</label>
                            <textarea name="body" id="" cols="30" rows="10" class="textarea"></textarea>
                        </div>

                        <input type="submit" class="btn btn-green" name="publish" value="Publier">
                        <input type="submit" class="btn btn-green" name="draft" value="Enregistrer comme brouillon">

                        {{--<button type="submit" class="btn btn-green">Enregistrer</button>
                        <button type="subm">Enregistrer comme brouillon</button>--}}
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
