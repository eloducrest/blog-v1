@extends('admin')

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center">
            <div class="col-6">
                @if (session('success_delete_comment'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success_delete_comment') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-12">
            <h1>Commentaires à vérifier :</h1>
        </div>
        <div class="col-12">
            <div class="card">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    <br/>
                @endif
                <div class="card-body">
                    <table class="table table-bordered sortDataTable">

                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>@mail</th>
                            <th>Article</th>
                            <th>Commentaire</th>
                            <th>Commentaire parent</th>
                            <th>Cree le</th>
                            <th class="text-center">Voir</th>
                            <th class="text-center">Valider</th>
                            <th class="text-center">Supprimer</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($commentsToValidate as $comment)
                            <tr>
                                <td>{{ $comment->id }}</td>
                                <td>{{ $comment->name }}</td>
                                <td>{{ $comment->email }}</td>
                                <td>{{ $comment->post->title }}</td>
                                <td>{{ $comment->body }}</td>
                                <td>{{ $comment->parent_id }}</td>
                                <td>{{ $comment->created_at }}</td>
                                <td class="text-center">
                                    <a href="/post/{{ $comment->post->id }}" class="btn">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <form action="verif-comment/{{$comment->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment confirmer le commentaire belle gosse ?')" class="d-inline">
                                        {{ csrf_field() }}
                                        <button class="btn" type="submit">
                                            <i class="fas fa-check"></i>
                                        </button>
                                    </form>
                                </td>
                                <td class="text-center">
                                    <form action="delete-comment/{{$comment->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment delete le commentaire belle gosse ?')" class="d-inline">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button class="btn" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-12">
            <h1>Tous les commentaires</h1>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered sortDataTable">

                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Article</th>
                            <th>Commentaire</th>
                            <th>Cree le</th>
                            <th class="text-center">Voir</th>
                            <th class="text-center">Valider</th>
                            <th class="text-center">Supprimer</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($allComments as $comment)
                            <tr>
                                <td>{{ $comment->id }}</td>
                                <td>{{ $comment->post->title }}</td>
                                <td>{{ $comment->body }}</td>
                                {{-- <td>{{ $comment->parent_id->body }}</td>--}}
                                <td>{{ $comment->created_at }}</td>
                                <td class="text-center">
                                    <a href="/post/{{ $comment->post->id }}" class="btn">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <form action="verif-comment/{{$comment->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment confirmer le commentaire belle gosse ?')" class="d-inline">
                                        {{ csrf_field() }}
                                        <button class="btn" type="submit">
                                            <i class="fas fa-check"></i>
                                        </button>
                                    </form>
                                </td>
                                <td class="text-center">
                                    <form action="delete-comment/{{$comment->id}}" method="post" onsubmit="return confirm('Veux-tu vraiment delete le commentaire belle gosse ?')" class="d-inline">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button class="btn" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
