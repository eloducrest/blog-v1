@extends('admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Liste de toutes les personnes ayant laissé un commentaire :</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                        <br/>
                    @endif
                    <div class="card-body">
                        <table class="table table-bordered sortDataTable">

                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Adresse @</th>
                                    <th>1<sup>er</sup> com</th>
                                    <th>Dernier com</th>
                                    <th>Nb de com </th>
                                    <th class="text-center">Voir</th>
                                    <th class="text-center">Supprimer</th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach($uniqueUserComment as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->lastComment->created_at }}</td>
                                    <td>{{ $user->totalCommentByUser }}</td>
                                    <td class="text-center">
                                        <a href="#" class="btn">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <form action="" method="post" onsubmit="return confirm('Veux-tu vraiment delete le post belle gosse ?')" class="d-inline">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <button class="btn" type="submit">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <h1>Liste de toutes les personnes ayant envoyés un message :</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    <br/>
                @endif
                <div class="card-body">
                    <table class="table table-bordered sortDataTable">

                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Adresse @</th>
                                <th>1<sup>er</sup> mail</th>
                                <th>Dernier mail</th>
                                <th>Nb de mail </th>
                                <th class="text-center">Voir</th>
                                <th class="text-center">Supprimer</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($uniqueUserContact as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->lastContact->created_at }}</td>
                                <td>{{ $user->totalContactByUser }}</td>
                                <td class="text-center">
                                    <a href="#" class="btn">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <form action="" method="post" onsubmit="return confirm('Veux-tu vraiment delete le post belle gosse ?')" class="d-inline">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button class="btn" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
