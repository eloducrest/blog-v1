<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="{{ asset('img/favicon.ico') }}">

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="robots" content="noindex, nofollow">

    <title>Admin | Faufau</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/adminlte/css/adminlte.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- multi select creation -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
    <script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>

    <!-- styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>


<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item-admin">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item-admin d-none d-sm-inline-block">
                <a href="{{ route('homeadmin') }}"
                   class="nav-link {{ request()->routeIs('homeadmin') ? 'active' : ''  }}">Accueil</a>
            </li>
            <li class="nav-item-admin d-none d-sm-inline-block">
                <a href="{{ route('allusers') }}" class="nav-link {{ request()->routeIs('allusers') ? 'active' : ''}}">Users</a>
            </li>
            <li class="nav-item-admin d-none d-sm-inline-block">
                <a href="{{ route('comments') }}" class="nav-link {{ request()->routeIs('comments') ? 'active' : ''}}">Moderation</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item-admin dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>

            <li class="dropdown user user-menu open">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="true">
                    <span class="hidden-xs">Faustine Clerc</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{ asset('img/IMG-2604.jpg') }}" class="img-circle" alt="User Image">

                        <p>Faustine Clerc</p>
                    </li>
                    <!-- Menu Body -->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="float-left">
                            <a href="/" class="btn btn-default btn-flat">Blog</a>
                        </div>
                        <div class="float-right">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

        </div>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4 position-fixed">
        <!-- Brand Logo -->
        <a href="{{ route('homeadmin') }}" class="brand-link">
            <img src="{{ asset('img/logo-de-faustine-clerc.png') }}" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light" style="color: #343a40">Faufau</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar-mini sidebar-collapse">

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column text-center" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    <li class="nav-item-admin mb-3">
                        <a href="{{ route('allposts') }}"
                           class="nav-link {{ request()->routeIs('allposts') ? 'active' : ''}}">
                            <i class="nav-icon fas fa-newspaper"></i>
                        </a>
                    </li>

                    <li class="nav-item-admin mb-3">
                        <a href="{{ route('create') }}"
                           class="nav-link  {{ request()->routeIs('create') ? 'active' : ''  }}">
                            <i class="nav-icon fas fa-pen"></i>
                        </a>
                    </li>

                    <li class="nav-item-admin mb-3">
                        <a href="{{ route('allsettings') }}"
                           class="nav-link  {{ request()->routeIs('allsettings') ? 'active' : ''  }}">
                            <i class="nav-icon fas fa-cog"></i>
                        </a>
                    </li>

                    <li class="nav-item-admin mb-3">
                        <a href="{{ route('show_insta') }}"
                           class="nav-link  {{ request()->routeIs('show_insta') ? 'active' : ''  }}">
                            <i class="fab fa-instagram" style="font-size: 24px;"></i>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="mb-5" style="margin-left: 70px">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte//plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/js/demo.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- Select 2 -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

{{--<script src="="{{ asset('js/app.js') }}""></script>--}}



<!-- page script CREATE -->
<script>
    $(document).ready(function() {
        $(".js-example-tags").select2({
            tags: true,
            selectOnClose: true,

            createTag: function (tag) {
                return {
                    id: tag.term,
                    text: tag.term,
                    // add indicator:
                    isNew : true
                };
            },

        }).on("select2:select", function(e) {
            let _ = this;
            if(e.params.data.isNew){
                // append the new option element prenamently:
                // store the new tag:
                data = new FormData();
                data.append( "_token", "{{ csrf_token() }}")
                data.append( "name", e.params.data.text)
                $.ajax({
                    data: data,
                    type: "POST",
                    url: "/admin/store_tag_in_post_create",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(tag) {
                        $(_).find('[value="'+e.params.data.text+'"]').replaceWith('<option selected value="'+tag.id+'">'+e.params.data.text+'</option>');
                    },

                });

            }
        });
    });

    $(function () {
        // Summernote
        $('.textarea').summernote({
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            height: 500,

            callbacks: {
                onImageUpload: function(files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                }
            }
        })



        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append( "_token", "{{ csrf_token() }}")
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "/admin/saveimage",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    var image = $('<img>').attr('src', url);
                    $('.textarea').summernote("insertNode", image[0]);
                },

            });
        }
    })
</script>

<!-- page script EDIT DATATABLE -->
<script>
    $(function () {
        $('.sortDataTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>

<!--insta img-->
<script>

    function resizeWindow() {
        var cats = document.querySelectorAll(".img-cropped");
        var width = cats[0].clientWidth;
        for (var i = 0; i < cats.length; i++) {
            cats[i].style.height = width + "px";
        }
    }
    resizeWindow();
    window.onresize = resizeWindow;
</script>

<!-- SCRIPT FOR HIDE & SHOW REPLY COMMENTS -->
<script>
  function showFt(i, name) {
    var div = document.getElementById('reply-form');
    var add_comment_form = document.getElementById('add-comment');

    var parent_id_input = document.getElementById('parent_id');
    var response_at = document.getElementById('response_at');
    parent_id_input.value = i;
    response_at.textContent = 'repondre à ' + name;

    add_comment_form.style.display = "none";
    document.getElementById(i).appendChild(div);
    div.style.display = "block";
  }

  function hideFt() {
    var close = document.getElementById('reply-form');
    var add = document.getElementById('add-comment');

    close.style.display = "none";
    add.style.display = "block";
  }
</script>

</body>
</html>
