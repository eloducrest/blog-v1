<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-KJDM86Z');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-ZC4JHS809L"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-ZC4JHS809L');
    </script>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="{{ asset('img/favicon.ico') }}">

    <title>{{ isset($title) ? config('app.name') . ' | ' . $title : config('app.name') }}</title>

    <!-- Meta données -->
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Faustine Clerc">
    <meta name="copyright" content="Eloi du Crest">
    <meta name="designer" content="Eloi du Crest">
    <meta name="Subject" content="Mode, Décoration, Restaurants, Shopping, Beauté">
    <meta name="robots" content="index, follow, max-image-preview:large">
    <meta name="Rating" content="general">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="{{ url()->current() }}">



    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!--Font Awesome -->
    <script src="https://kit.fontawesome.com/1f2873356d.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>



    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KJDM86Z"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
        $categories = \App\Models\Category::where('parent_id', '=', null)->with('childs')->get();

        if (!isset($catId))
            {
                $catId = null;
            }
    ?>

    <header class="site-header">
        <div class="container">
            <div class="site-branding">
                <div class="site-title text-center">
                    <a href="{{ route('home') }}" title="Faustine Clerc" rel="home">
                        <img src="{{ asset('img/logo-de-faustine-clerc.png') }}" alt="Logo de Faustine Clerc">
                    </a>
                </div>
            </div>
        </div>
    </header>

    <div id="myHeader" class="site-nav">
        <div class="container-fluid">
            <nav class="site-menu navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="{{ asset('img/logo-de-faustine-clerc.png') }}" alt="Logo de Faustine Clerc">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="menu-bar collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="nav-menu navbar-nav">
                        <li id="home-icon" class="bottomMenu hide nav-item">
                            <a href="{{ route('home') }}">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>

                        @foreach($categories as $category)
                            @if(count($category->childs) === 0)
                                <li class="nav-item {{ $catId === $category->id ? 'active' : ''  }}" >
                                    <a href="/{{ $category->category_slug }}">{{ $category->name }}</a>
                                </li>
                            @else
                                <li class="nav-item dropdown {{ ($catId === $category->id  ) ? 'active' : ''  }}">
                                    <a class="dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $category->name }}</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown03">
                                        @foreach($category->childs as $subCat)
                                            <a class="dropdown-item" href="/{{ $subCat->category_slug }}">{{ $subCat->name }}</a>
                                        @endforeach
                                    </div>
                                </li>
                            @endif
                        @endforeach

                        <li class="social-nav">
                            <a href="https://www.instagram.com/faustine.clerc/" target="_blank" aria-label="instagram" title="instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.pinterest.fr/Faustine_Clerc/" target="_blank" aria-label="pinterest" title="pinterest">
                                <i class="fa fa-pinterest"></i>
                            </a>
                            <a href="{{ route('contact') }}">
                                <i class="fa fa-envelope"></i>
                            </a>
                            {{--<a class="toggle-search" style="cursor: pointer;">
                                <i class="fa fa-search"></i>
                            </a>
                            <div>
                                <form class="row search-input" id="demo-2" style="margin-left: 10px; display: none;">
                                    @csrf
                                    <input class="col-lg-12" type="search" placeholder="Rechercher">
                                </form>
                            </div>--}}
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>




    <main class="main-content">
        @yield('content')
    </main>



    <footer id="foot" class="site-footer">
        <div class="info-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="info text-center">&copy; Copyright {{ date('Y') }} &middot; Faustine Clerc &middot; tous droits reservés | <a href="{{ route('mentionslegales') }}">Mentions légales</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- POPPER.JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <!-- BOOTSTRAP.JS -->

    <script src="{{ asset('js/app.js') }}"></script>


    <!-- SCRIPT FOR HIDE & SHOW REPLY COMMENTS -->
    <script>
        function showFt(i, name) {
            var div = document.getElementById('reply-form');
            var add_comment_form = document.getElementById('add-comment');

            var parent_id_input = document.getElementById('parent_id');
            var response_at = document.getElementById('response_at');
            parent_id_input.value = i;
            response_at.textContent = 'repondre à ' + name;

            add_comment_form.style.display = "none";
            document.getElementById(i).appendChild(div);
            div.style.display = "block";
        }/*;*/

        function hideFt() {
            var close = document.getElementById('reply-form');
            var add = document.getElementById('add-comment');

            close.style.display = "none";
            add.style.display = "block";
        }
    </script>

    <!--insta img-->
    <script>

      function resizeWindow() {
        var cats = document.querySelectorAll(".img-cropped");
        var width = cats[0].clientWidth;
        for (var i = 0; i < cats.length; i++) {
          cats[i].style.height = width + "px";
        }
      }
      resizeWindow();
      window.onresize = resizeWindow;
    </script>

    <!-- SCRIPT FOR STICKY SUMMARY POST -->
    <script>
      let positionSummary = 0;
      let limitPosition = 70;
      let width = 0;


      function stickySummary() {
        positionSummary = document.querySelector('.summary-post').getBoundingClientRect().y;
        sumPost = document.querySelector('.sum-post');
        width = screen.width;
        if (width > 767) {
          if (positionSummary > limitPosition) {
            sumPost.classList.add("sticky-sum")
          } else {
            sumPost.classList.remove("sticky-sum")
          }
        } else {
          sumPost.classList.add("sticky-sum")
        }
      }

      window.addEventListener('scroll', stickySummary());
    </script>

    <script>
      function goToSection(i) {
        document.getElementById(i).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
      }
    </script>

    <script>
      function addHighlight(i) {
        this.item = document.querySelectorAll(".nav-item-post");
        this.item[i].classList.add("active-summary");
      };

      function  removeHighlight(i) {
        this.item = document.querySelectorAll(".nav-item-post");
        this.item[i].classList.remove("active-summary");
      };

      function highlightNav() {
        let panel = document.querySelectorAll(".article-pp");

        if (panel[0].getBoundingClientRect().y > -136) {
          removeHighlight(0);
          removeHighlight(1);
        }
        if (panel[0].getBoundingClientRect().y <= -136) {
          addHighlight(0);
          removeHighlight(1);
        }
        if (panel[1].getBoundingClientRect().y <= -5389) {
          removeHighlight(0);
          addHighlight(1);
        }
      };

      window.addEventListener('scroll', highlightNav);
    </script>

    </body>
</html>
