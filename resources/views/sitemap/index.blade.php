@php echo '<?xml version="1.0" encoding="UTF-8"?>' @endphp

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <url>
        <loc>https://faustineclerc.fr/</loc>
        <lastmod>2021-07-19T17:41:29+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
    </url>

    @foreach($allcategories as $category)
        @if(count($category->childs) === 0)
            <url>
                <loc>{{ 'https://faustineclerc.fr/' . $category->category_slug }}</loc>
                <lastmod>{{ $category->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                <changefreq>weekly</changefreq>
                <priority>0.8</priority>
            </url>
        @endif
    @endforeach

    @foreach ($posts as $post)
        <url>
            <loc>{{ 'https://faustineclerc.fr/' . $post->category->category_slug . '/' . $post->slug}}</loc>
            <lastmod>{{ $post->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach

        <url>
            <loc>https://faustineclerc.fr/contact</loc>
            <lastmod>2021-07-19T17:41:29+00:00</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.8</priority>
        </url>
        <url>
            <loc>https://faustineclerc.fr/mentions-legales</loc>
            <lastmod>2021-07-19T17:41:29+00:00</lastmod>
            <changefreq>yearly</changefreq>
            <priority>0.5</priority>
        </url>


</urlset>