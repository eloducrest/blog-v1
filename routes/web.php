<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\InstagramController;
use App\Http\Controllers\admin\ModerationController;
use App\Http\Controllers\admin\PostController;
use App\Http\Controllers\admin\SettingController;
use App\Http\Controllers\front\CommentController;
use App\Http\Controllers\front\ContactUsFormController;
use App\Http\Controllers\front\FrontController;
use App\Http\Controllers\sitemap\SitemapController;
use Illuminate\Support\Facades\Route;
require __DIR__.'/auth.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







/*
|--------------------------------------------------------------------------
| SITEMAP Routes
|--------------------------------------------------------------------------
|
|
|
*/
Route::get('/sitemap', [SitemapController::class, 'index'])->name('sitemap.index');









/*
|--------------------------------------------------------------------------
| ADMIN Routes
|--------------------------------------------------------------------------
|
|
|
*/
Route::prefix('/admin')->middleware('auth')->namespace('admin')->group(function () {

    // HOME ADMIN
    // HOME ADMIN
    Route::get('/', [AdminController::class, 'index'])->name('homeadmin');

    // CRUD POSTS
    // CRUD POSTS
    Route::get('/allposts', [PostController::class, 'allposts'])->name('allposts'); // allposts datatable admin route
    Route::get('/post/create', [PostController::class, 'create'])->name('create'); // create new post route
    Route::post('/post', [PostController::class, 'store'])->name('upload.post'); // upload post route
    Route::get('/post/{post}/edit', [PostController::class, 'edit']); // edit post route
    Route::get('/post/{post}', [PostController::class, 'show']); // see one post route
    Route::put('/post/{post}', [PostController::class, 'update']); // update post route
    Route::delete('/post/{post}', [PostController::class, 'destroy']);

    // UPLOAD IMAGE
    // UPLOAD IMAGE
    Route::post('/saveimage', [PostController::class, 'uploadImages']);

    // SETTINGS ROUTES
    // SETTINGS ROUTES
    Route::get('/settings', [SettingController::class, 'allsettings'])->name('allsettings'); // settings admin route
    // CATEGORIES
    Route::post('/store-cat', [SettingController::class, 'store_cat'])->name('upload.category'); // create new category
    Route::delete('/delete-cat/{category}', [SettingController::class, 'destroy_cat']);
    // TAGS
    Route::post('/store-tag', [SettingController::class, 'store_tag'])->name('upload.tag'); // create new tag
    Route::post('/store_tag_in_post_create', [SettingController::class, 'store_tag_in_post_create'])->name('create.store_tag_in_post_create'); // create new tag
    Route::delete('/delete-tag/{tag}', [SettingController::class, 'destroy_tag']);

    // INSTAGRAM ROUTES
    // INSTAGRAM ROUTES
    Route::get('/manage-insta', [InstagramController::class, 'show_insta'])->name('show_insta');
    Route::post('/store-insta', [InstagramController::class, 'store_insta'])->name('upload.insta');
    Route::delete('/delete-insta/{instagram}', [InstagramController::class, 'destroy_insta']);

    //MODERATION ROUTES
    //MODERATION ROUTES
    // COMMENTS
    Route::get('/moderation-comments', [ModerationController::class, 'comments'])->name('comments');
    Route::delete('/delete-comment/{comment}', [ModerationController::class, 'destroy']);
    Route::post('/verif-comment/{comment}', [ModerationController::class, 'verified']);
    // USERS
    Route::get('/moderation-allusers', [ModerationController::class, 'allusers'])->name('allusers');

});




/*
|--------------------------------------------------------------------------
|       FRONT ROUTES
|--------------------------------------------------------------------------
|
|
|
*/
    // HOME
Route::get('/', [FrontController::class, 'homeposts'])->name('home');

    // MENTIONS LEGALES
Route::get('/mentions-legales', [FrontController::class, 'mentionslegales'])->name('mentionslegales');

    // COMMENTS FOR ONE POST
Route::post('/comments/store', [CommentController::class, 'store'])->name('comment.store');

    // CONTACT
Route::get('/contact', [ContactUsFormController::class, 'createForm'])->name('contact');
Route::post('/contact', [ContactUsFormController::class, 'ContactUsForm'])->name('contact.store');

/*
|------------------
|       SLUG
|------------------
*/
    // SORTING POST BY CATEGORY
Route::get('/{category:category_slug}', [FrontController::class, 'postByCat']);

// GO TO ONE POST
Route::get('/{category:slug}/{post:slug}', [FrontController::class, 'show'])->name('post');





