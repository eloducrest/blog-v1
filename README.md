<h1><u>Upload images :</u></h1>
De base la max size d'une image est de 2mb.
Pour changer : <br>
<ul>
<li><code>sudo /etc/php/8.0/cli/php.init</code></li>
</ul>
Modifier les lignes suivantes :
<ul>
<li><code>upload_max_filesize = 64M</code></li>
<li><code>post_max_size = 64M</code></li>
<li><code>memory_limit = 256M</code></li>
</ul>
Si en local, faire :
<ul>
<li><code>sudo apt install php8.0-fpm -y</code></li>
<li><code>sudo service php8.0-fpm start</code></li>
<li><code>sudo service apache2 restart</code></li>
</ul>

<br>


<h1><u>Link storage</u></h1>
<p>pour l'upload des photos, il faut linker le dossier storage dans le dossier public :</p>
<code>php artisan storage:link</code>


<h1><u>INFO MAIL GUN</u></h1>
<ul>
<li>API key: 76aa4de37473cd6bf138e5d082c00c23-1d8af1f4-b86b29ff</li>
<li>API base URL: https://api.eu.mailgun.net/v3/faustineclerc.fr</li>
</ul>
