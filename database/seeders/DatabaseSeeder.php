<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::factory()
            ->count(1)
            ->create([
                'name' => 'Faustine Clerc',
                'email' => 'contact@faustineclerc.fr',
                'email_verified_at' => now(),
                'password' => '$2y$10$XxXq/AEQ07rdT0p2PW/kZuKYwVSKMtcoVgeESbgpGbipScoD62086',
            ]);
        User::factory()
            ->count(1)
            ->create([
                'name' => 'Eloi du Crest',
                'email' => 'eloiducrest92@gmail.com',
                'email_verified_at' => now(),
                'password' => '$2y$10$KcHFacpmqu69BvRRl25mSeS/2OrlEIKW6O16T8juMzpdcS8SlGTk6',
            ]);
        // \App\Models\User::factory(10)->create();
    }
}
